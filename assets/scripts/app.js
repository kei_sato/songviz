jQuery(function($) {
	var utils = {
		context : null,
		songBuffer : null,
		analyser : null,
		soundSource : null,
		canvas : null,

		init : function(){
			console.log('initing');

			window.addEventListener('load', this.audioInit(), false);

			this.loadSound('../data/needYa.mp3');

			//this.initOscill('../data/needYa.mp3');
			
		},

		audioInit : function(){
			try {
	    		// Fix up for prefixing
	    		this.context = new (window.AudioContext || window.webkitAudioContext)();
			 }
		  	catch(e) {
		    	alert('Web Audio API is not supported in this browser');
		  	}
		},

		initOscill : function(){
			//by this time only the audio context has been set up

			console.log('Init Oscilliscope function');

			console.log(this.context);

			console.log(this.songBuffer);

		//	var song = this.context.createBufferSource(this.songBuffer);

		//	var source = this.context.createMediaStreamDestination(song);


			var source = this.context.createBufferSource();
			
			console.log(source);

			source.connect(this.analyser);


			this.analyser = this.context.createAnalyser();

			//var source = this.context.createMediaStreamDestination();


			this.soundSource.connect(this.analyser);



			this.analyser.fftSize = 2048;

			var bufferLength = this.analyser.fftSize;
			//var bufferLength = this.analyser.frequencyBinCount;

			this.dataArray = new Uint8Array(bufferLength);




			console.log(this.dataArray);

			this.analyser.getByteTimeDomainData(this.dataArray);

			console.log(this.dataArray);


			var that = this;

			this.canvas = document.getElementById('myCanvas');
			this.canvasCtx = this.canvas.getContext('2d');

			this.canvasCtx.clearRect(0, 0, _width, _height);

			var _width = this.canvas.width;
			var _height = this.canvas.height;

			function draw() {
				//console.log(that.dataArray);

				drawVisual = requestAnimationFrame(draw);


				that.canvasCtx.fillStyle = 'rgb(255, 255, 255)';
				that.canvasCtx.fillRect(0, 0, _width, _height);

				that.canvasCtx.lineWidth = 2;
				that.canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

				that.canvasCtx.beginPath();

				var sliceWidth = _width * 1.0 / bufferLength;
				var x = 0;

				//console.log(that.dataArray);

				for (i = 0; i < bufferLength; i++) {
					var v = that.dataArray[i]/128.0;
					var y = v * _height/2;
					//console.log(y);

					if (i === 0){
						that.canvasCtx.moveTo(x, y);
					} else {
						that.canvasCtx.lineTo(x, y);
					}

					x += sliceWidth;
					//console.log('x' + x + ' y ' + y );
				}

				that.canvasCtx.lineTo(that.canvas.width, that.canvas.height/2);
				that.canvasCtx.stroke();
			}

			draw();
		},

		loadSound : function(url){
			console.log('loadSound function');

			var request = new XMLHttpRequest();
			var that = this;
		  	request.open('GET', url, true);
		  	request.responseType = 'arraybuffer';

			// Decode asynchronously
			request.onload = function() {
				that.context.decodeAudioData(request.response, function(buffer) {
					that.songBuffer = buffer;
					that.playSound(that.songBuffer);
					that.initOscill();

				}, function(){ 
					console.log('error error'); 
				});
			}
  			
  			request.send();
	 
		}, 

		playSound : function(buffer){
			console.log('play sound function');

			this.soundSource = this.context.createBufferSource();	// creates a sound source
  			this.soundSource.buffer = buffer;						// tell the source which sound to play
			this.soundSource.connect(this.context.destination);  	// connect the source to the context's destination (the speakers)
			this.soundSource.loop = true;
			//this.soundSource.start(0);

		}


	}

	utils.init();
});