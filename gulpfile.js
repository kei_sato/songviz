var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();

var paths = {
	'bower': './bower_components',
	'assets': './assets'
}

gulp.task('serve', function(){
	browserSync.init({
		server: {
			baseDir: './public'
		}
	});

	// gulp.watch(paths.assets + '/styles/**/*.scss',['styles']);
	 gulp.watch(paths.assets + '/scripts/*.js',['scripts']);
	// gulp.watch(paths.assets + '/images/*' , ['images']);
	gulp.watch(paths.assets + '/pages/*' , ['pages']);
	// gulp.watch(paths.assets + '/data/*', ['data']);


 	gulp.watch([paths.assets + '/pages/*', paths.assets + '/scripts/*']).on('change', browserSync.reload);
});

gulp.task('pages', function(){
	//console.log('pages');
	return gulp.src([paths.assets + '/pages/*']).pipe(gulp.dest('./public'));

});

gulp.task('styles', function(){
	return gulp.src([
		paths.assets + '/styles/app.scss'
	])
	.pipe(sass({
		includePaths: [
			paths.bower + '/foundation/scss',
			paths.bower + '/slick.js/slick/'
		]
	}))
	.pipe(concat('app.css'))
	.pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function(){
	gulp.src([
		paths.bower + '/jquery/dist/jquery.js',
		paths.bower + '/foundation/js/foundation.js',
	])
	.pipe(concat('base.js'))
	.pipe(gulp.dest('./public/js'));

	gulp.src(paths.bower + '/modernizr/modernizr.js').pipe(gulp.dest('./public/js'));

	gulp.src([
		paths.assets + '/scripts/paperjs/paper-full.js', 
		paths.assets + '/scripts/app.js'])
	.pipe(concat('app.js'))
	.pipe(gulp.dest('./public/js'));

});



gulp.task('default', ['serve']);



